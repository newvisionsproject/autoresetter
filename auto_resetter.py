#!/usr/bin/python3
import time
import pigpio
import socket
from datetime import datetime

REMOTE_SERVER = "www.google.com"

rasp = pigpio.pi()

def is_connected():
  try:
    # see if we can resolve the host name -- tells us if there is
    # a DNS listening
    host = socket.gethostbyname(REMOTE_SERVER)
    # connect to the host -- tells us if the host is actually
    # reachable
    s = socket.create_connection((host, 80), 2)
    return True
  except:
     pass
  return False


#init
rasp.write(21, 1)
rasp.write( 5, 0)


while True:
	print("waiting 15min for check")
	time.sleep(15)

	print("normal connection check")
	online = is_connected()

	if(not online):
		print("error on connection detected...")
		
	counter = 0
	for i in range(0, 3):
		time.sleep(5)
		online = is_connected()
		if(not online):
			print("error enduring for {} times".format(i+1))
			counter = counter + 1
			
		if(counter == 3):
			print("turning ON")
			rasp.write(21,1);
			rasp.write(5,0) # led off
			
			print("turning OFF")
			rasp.write(21,0)
			rasp.write(5, 1) # led on
			time.sleep(5)
			
			print("turning ON")
			rasp.write(21,1)
			rasp.write(5, 0) # led off
			print ("next check in 5 hours")
			time.sleep(30)
		else:
			print("online at {0:%H:%M:%S}".format(datetime.now()))
